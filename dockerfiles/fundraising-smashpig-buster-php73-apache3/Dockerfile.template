# PHP and Apache image FR-Tech development environment, for smashpig

FROM {{ "fundraising-buster-php73" | image_tag }} AS fundraising-buster-php73

# Note that WMF APT key is pre-installed.
# See: https://wikitech.wikimedia.org/wiki/APT_repository#Security

# Install apache from debian packages

{% set packages|replace('\n', ' ') -%}
apache2
libapache2-mod-php
{%- endset -%}

RUN {{ packages | apt_install }}

# Remove default index.html, and enable some apache modules.

RUN rm /var/www/html/index.html \
    && a2enmod rewrite \
    && a2enmod http2 \
    && a2enmod cache \
    && a2enmod setenvif \
    && a2enmod headers

# Copy Apache config into image

COPY ./ports.conf /etc/apache2/ports.conf
COPY ./envvars /etc/apache2/envvars
COPY ./000-default.conf /etc/apache2/sites-available/

# TODO Move or correct these settings, taken from standard Mediawiki dev images
RUN echo 'opcache.file_update_protection=0\n\
    opcache.memory_consumption=256\n\
    opcache.max_accelerated_files=24000\n\
    opcache.max_wasted_percentage=10\n\
    opcache.fast_shutdown=1\n\
    zlib.output_compression=On\n\
    upload_max_filesize=100M\n\
    post_max_size=100M\n' | tee -a /etc/php/7.3/apache2/php.ini /etc/php/7.3/cli/php.ini

# set permissions to let entrypoint.sh links to xdebug config files on the host
RUN chmod a+rwx /etc/php/7.3/cli/conf.d/ \
    && chmod a+rwx /etc/php/7.3/apache2/conf.d/ \

    # xdebug-common.ini will be written by entrypoint.sh every time the container starts, to set
    # xdebug.remote_host to the current host IP.
    && ln -s /srv/config/internal/xdebug-common.ini /etc/php/7.3/cli/conf.d/25-common-xdebug.ini \
    && ln -s /srv/config/internal/xdebug-common.ini /etc/php/7.3/apache2/conf.d/25-common-xdebug.ini \

    # Link from expected SmashPig config location to a location writable by non-root.
    # (Another symlink from this location to an actual Smashpig config shared from the host
    # will be created in the entrypoint script. We do this to allow determination of which
    # config to use based on an environment variable, and avoid making all of /etc/
    # writable.)
    && ln -s /srv/config/internal/smashpig /etc/smashpig

# Default values for some environment variables used in entrypoint script
ENV FR_DOCKER_SERVICE_NAME=smashpig
ENV FR_DOCKER_SMASHPIG_CONFIG_DIR=smashpig

# TODO Health check, including rsyslogd

EXPOSE 9001

COPY entrypoint.sh /srv/entrypoint.sh

ENTRYPOINT ["/bin/bash", "/srv/entrypoint.sh"]
