#!/bin/bash

# Create rsyslog conf and common xdebug conf
/srv/create-docker-rsyslog-conf.sh
/srv/create-xdebug-common-ini.sh

# /srv/config/exposed appears on the host as config. The files we link to
# below should be provided by the host. (In the case of fundraising-dev, they
# are written by setup.sh or provided as part of the fundraising-dev git repo.)

ln -sf /srv/config/exposed/${FR_DOCKER_SERVICE_NAME}/xdebug-web.ini /etc/php/7.3/apache2/conf.d/30-xdebug-web.ini
ln -sf /srv/config/exposed/${FR_DOCKER_SERVICE_NAME}/xdebug-cli.ini /etc/php/7.3/cli/conf.d/30-xdebug-cli.ini

/usr/sbin/rsyslogd -f /srv/config/internal/docker-rsyslog.conf -iNONE && \
	/usr/sbin/apache2ctl -D FOREGROUND
